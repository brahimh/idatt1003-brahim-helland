# Portfolio project IDATT1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Brahim Helland"  
CANDIDATE NUMBER = "10034"

## Project description

This project is a part of the course IDATT1003 Programming 1 at NTNU. It includes a program that can be used to manage 
train departures from a train station. The program has the ability to add new departures, search for departures, display 
departures, assign delays and track info, etc. The program is written in Java and has a text-based user interface 
through the console.

[//]: # (TODO: Write a short description of your project/product here.)

## Project structure

The project is structured by using packages. Within the 
[edu.ntnu.stud.idatt1003.brahimh](src/main/java/edu/ntnu/stud/idatt1003/brahimh) 
package there are four sub-packages: 
[app_launcher](src/main/java/edu/ntnu/stud/idatt1003/brahimh/app_launcher),
[model](src/main/java/edu/ntnu/stud/idatt1003/brahimh/model), 
[view](src/main/java/edu/ntnu/stud/idatt1003/brahimh/view) 
and 
[model_tests](src/test/java/edu/ntnu/stud/idatt1003/brahimh/model_tests).
The app_launcher-package contains the
main class of the program. The model-package contains all the classes that are used to model the data of the program.
The view-package contains all the classes that are used to display the data of the program. The model_tests-package
contains all the JUnit test-classes for the model-classes.

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)

## Link to repository

[//]: # (TODO: Include a link to your repository here.)
Here you can find a link to the GitLab repository that holds the project: https://gitlab.stud.idi.ntnu.no/brahimh/idatt1003-brahim-helland.

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)

The program is run from the 
[TrainDispatchApp.java](src/main/java/edu/ntnu/stud/idatt1003/brahimh/app_launcher/TrainDispatchApp.java)
file. All input and output is done through the console. 
The program is expected to run until the user exits the program 
which is done by selecting the "Exit program" option in the user interface.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)

There are three test-classes in this project found in the 
["model_tests"-package](src/test/java/edu/ntnu/stud/idatt1003/brahimh/model_tests). 
Each test-class tests a different class in the project. 

