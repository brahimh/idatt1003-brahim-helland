package edu.ntnu.stud.idatt1003.brahimh.model_tests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.idatt1003.brahimh.model.CurrentTimeHandler;
import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Class to test the CurrentTimeHandler class.
 *
 * @author Brahim Helland
 * @version 1.0
 */
public class CurrentTimeHandlerTest {

  /**
   * Nested class to do negative tests on the CurrentTimeHandler constructor.
   */
  @Nested
  @DisplayName("Negative tests TrainTime constructor, throws correct "
      + "IllegalArgumentException or DateTimeException on invalid input")
  class MethodsThrowExceptions {
    @Test
    @DisplayName(
        "TrainTime constructor throws IllegalArgumentException  on null time")
    void testConstructorOnNullTime() {
      assertThrows(
          NullPointerException.class, () -> new CurrentTimeHandler(null));
    }

    @Test
    @DisplayName("TrainTime constructor"
        + "throws IllegalArgumentException on time before current time")
    void testConstructorOnTimeBeforeCurrentTime() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(12, 30));
      LocalTime testTime = LocalTime.of(12, 29);
      assertThrows(IllegalArgumentException.class,
          () -> time.setTime(testTime));
    }

    @Test
    @DisplayName("TrainTime constructor "
        + "throws IllegalArgumentException on negative time.")
    void testConstructorOnNegativeTime() {
      assertThrows(java.time.DateTimeException.class,
          () -> new CurrentTimeHandler(LocalTime.of(-1, 30)));
    }
  }

  /**
   * Nested class to do positive tests on the CurrentTimeHandler constructor.
   */
  @Nested
  @DisplayName("Positive tests TrainTime constructor,"
      + " throws no exceptions on valid input")
  class MethodsDoNotThrowExceptions {
    @Test
    @DisplayName("TrainTime constructor throws no exceptions on valid input")
    void testConstructorOnValidInput() {
      assertDoesNotThrow(() -> new CurrentTimeHandler(LocalTime.of(12, 30)));
    }

    /**
     * Test that setter throws no exceptions on valid input.
     */
    @Test
    @DisplayName("Setter throws no exception on valid input")
    void testUpdateTimeOnValidInput() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(12, 30));
      assertDoesNotThrow(() -> time.setTime(LocalTime.of(12, 31)));
    }
  }

  /**
   * Nested class to test the getter methods.
   */
  @Nested
  @DisplayName("Test getters")
  class TestGetters {

    /**
     * Test that getTime returns the correct time.
     */
    @Test
    @DisplayName("Test getTime")
    void testGetTime() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(12, 30));
      assertEquals(LocalTime.of(12, 30), time.getTime());
    }
  }

  @Nested
  @DisplayName("Test validate methods")
  class TestValidateMethods {

    /**
     * Test that validateTimeInput throws no exceptions on valid input.
     */
    @Test
    @DisplayName("Test validateTimeInput")
    void positiveTestValidateTimeInput() {
      assertDoesNotThrow(() -> CurrentTimeHandler.validateTimeInput("12:30"));
      assertDoesNotThrow(() -> CurrentTimeHandler.validateTimeInput("23:59"));
    }

    /**
     * Test that validateTimeInput throws DateTimeParseException on invalid
     *     input.
     */
    @Test
    @DisplayName("Test validateTimeInput throws "
        + "DateTimeParseException on invalid input")
    void negativeTestValidateTimeInput() {
      assertThrows(java.time.format.DateTimeParseException.class,
          () -> CurrentTimeHandler.validateTimeInput("12:3"));
      assertThrows(java.time.format.DateTimeParseException.class,
          () -> CurrentTimeHandler.validateTimeInput("12:3a"));
      assertThrows(java.time.format.DateTimeParseException.class,
          () -> CurrentTimeHandler.validateTimeInput("12:3:00"));
      assertThrows(java.time.format.DateTimeParseException.class,
          () -> CurrentTimeHandler.validateTimeInput("testtesttest"));
    }
  }
}
