package edu.ntnu.stud.idatt1003.brahimh.model_tests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.idatt1003.brahimh.model.CurrentTimeHandler;
import edu.ntnu.stud.idatt1003.brahimh.model.TrainDeparture;
import edu.ntnu.stud.idatt1003.brahimh.model.TrainRegister;
import java.time.LocalTime;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Class to test the TrainRegister class.
 *
 * @author Brahim Helland
 * @version 1.0
 */
class TrainRegisterTest {

  /**
   * Nested class to do negative tests on the TrainRegister constructor.
   */
  @Nested
  @DisplayName("Negative tests TrainRegister constructor,"
      + " throws IllegalArgumentException or "
      + "java.time.DateTimeException on invalid input")
  class ConstructorThrowsExceptions {

    /**
     * Test that TrainRegister constructor throws
     * IllegalArgumentException on null ArrayList.
     */
    @Test
    @DisplayName("TrainRegister constructor throws "
        + "IllegalArgumentException on null ArrayList")
    void testTrainRegisterConstructorOnNullArrayList() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(0, 0));
      assertThrows(IllegalArgumentException.class,
          () -> new TrainRegister(null, time));
    }

    /**
     * Test that TrainRegister constructor throws IllegalArgumentException
     * on null CurrentTimeHandler.
     */
    @Test
    @DisplayName("TrainRegister constructor throws "
        + "IllegalArgumentException on null CurrentTimeHandler")
    void testTrainRegisterConstructorOnNullCurrentTimeHandler() {
      ArrayList<TrainDeparture> testList = new ArrayList<>();
      assertThrows(IllegalArgumentException.class,
          () -> new TrainRegister(testList, null));
    }
  }

  /**
   * Nested class to do positive tests on the TrainRegister constructor.
   */
  @Nested
  @DisplayName("Positive tests for TrainRegister constructor,"
      + " throws no exceptions on valid input")
  class ConstructorDoesNotThrowExceptions {

    /**
     * Test that TrainRegister constructor throws no exceptions on valid input.
     */
    @Test
    @DisplayName("TrainRegister constructor throws "
        + "no exceptions on valid input")
    void testTrainRegisterConstructorOnValidInput() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(0, 0));
      assertDoesNotThrow(() -> new TrainRegister(new ArrayList<>(), time));
    }
  }

  /**
   * Nested class to do positive tests on the TrainRegister methods.
   */
  @Nested
  @DisplayName("Positive tests for TrainRegister methods")
  class MethodsDoNotThrowExceptions {

    /**
     * Test variable for the TrainRegister object for the
     * MethodsDoNotThrowExceptions class to use throughout tests.
     */
    private TrainRegister register;

    /**
     * Test variable for the CurrentTimeHandler object for the
     * MethodsDoNotThrowExceptions class to use throughout tests.
     */
    private CurrentTimeHandler time;

    /**
     * Test TrainDeparture object 1 to use in tests.
     */
    private TrainDeparture train1;

    /**
     * Test TrainDeparture object 2 to use in tests.
     */
    private TrainDeparture train2;

    /**
     * Test TrainDeparture object 3 to use in tests.
     */
    private TrainDeparture train3;

    /**
     * Test TrainDeparture object 4 to use in tests.
     */
    private TrainDeparture train4;

    /**
     * Test TrainDeparture object 5 to use in tests.
     */
    private TrainDeparture train5;


    @BeforeEach
    void init() {
      time = new CurrentTimeHandler(LocalTime.of(0, 0));
      register = new TrainRegister(new ArrayList<>(), time);
      train1 =
          new TrainDeparture(LocalTime.of(10, 20),
              "L4", 1, "Trondheim", 1, LocalTime.of(0, 10));
      train2 =
          new TrainDeparture(LocalTime.of(20, 10),
              "L4", 2, "Trondheim", 1, LocalTime.of(0, 0));
      train3 =
          new TrainDeparture(LocalTime.of(14, 25),
              "L21", 3, "Oslo", 3, LocalTime.of(0, 0));
      train4 =
          new TrainDeparture(LocalTime.of(17, 45),
              "L26", 4, "Bergen", 2, LocalTime.of(0, 5));
      train5 =
          new TrainDeparture(LocalTime.of(22, 20),
              "L4", 5, "Stavanger", 1, LocalTime.of(1, 30));
      register.registerNewTrainDeparture(train1);
      register.registerNewTrainDeparture(train2);
      register.registerNewTrainDeparture(train3);
      register.registerNewTrainDeparture(train4);
      register.registerNewTrainDeparture(train5);
    }

    /**
     * Test that registerNewTrainDeparture throws no exceptions on valid input.
     */
    @Test
    @DisplayName("registerNewTrainDeparture throws "
        + "no exceptions on valid input")
    void testRegisterNewTrainDepartureOnValidInput() {
      int sizeBefore = register.getTrainDepartures().size();
      TrainDeparture train6 =
          new TrainDeparture(LocalTime.of(10, 20),
              "L4", 6, "Trondheim", 1, LocalTime.of(0, 10));
      assertDoesNotThrow(() -> register.registerNewTrainDeparture(train6));
      assertEquals(sizeBefore + 1, register.getTrainDepartures().size());
    }

    /**
     * Test that getTrainDepartureByTrainNumber
     * returns correct train on valid input.
     */
    @Test
    @DisplayName("getTrainDepartureByTrainNumber "
        + "returns correct train on valid input")
    void testGetTrainDepartureByTrainNumberOnValidInput() {
      assertEquals(train1, register.getTrainDepartureByTrainNumber(1));
      assertEquals(train2, register.getTrainDepartureByTrainNumber(2));
      assertEquals(train3, register.getTrainDepartureByTrainNumber(3));
      assertEquals(train4, register.getTrainDepartureByTrainNumber(4));
      assertEquals(train5, register.getTrainDepartureByTrainNumber(5));
    }

    /**
     * Test that getTrainDepartureByDestination returns
     * correct trains on valid input.
     */
    @Test
    @DisplayName("getTrainDeparturesByDestination "
        + "returns correct values on valid input")
    void testGetTrainByDestinationOnValidInput() {
      ArrayList<TrainDeparture> trondheimTrains;
      trondheimTrains = new ArrayList<>();
      trondheimTrains.add(train1);
      trondheimTrains.add(train2);
      assertEquals(trondheimTrains,
          register.getTrainDeparturesByDestination("Trondheim"));
      assertEquals(trondheimTrains,
          register.getTrainDeparturesByDestination("TRONDHEIM"));
      assertEquals(trondheimTrains,
          register.getTrainDeparturesByDestination("trondheim"));
      assertEquals(trondheimTrains,
          register.getTrainDeparturesByDestination("tRoNdHeIm"));

    }

    /**
     * Test that getTrainDeparturesByTrainLine returns correct
     * values on valid input.
     */

    @Test
    @DisplayName("getTrainDeparturesByTrainLine "
        + "returns correct values on valid input")
    void testGetTrainDeparturesByTrainLineOnValidInput() {
      ArrayList<TrainDeparture> lineL4Trains;
      lineL4Trains = new ArrayList<>();
      lineL4Trains.add(train1);
      lineL4Trains.add(train2);
      lineL4Trains.add(train5);
      assertEquals(lineL4Trains, register.getTrainDeparturesByTrainLine("L4"));

    }

    /**
     * Test that getTrainDeparturesByTrackNumber returns correct
     * values on valid input.
     */

    @Test
    @DisplayName("getTrainDeparturesByTrackNumber "
        + "returns correct values on valid input")
    void testGetTrainDeparturesByTrackNumberOnValidInput() {
      ArrayList<TrainDeparture> track1Trains;
      track1Trains = new ArrayList<>();
      track1Trains.add(train1);
      track1Trains.add(train2);
      track1Trains.add(train5);
      assertEquals(track1Trains, register.getTrainDeparturesByTrackNumber(1));
    }

    /**
     * Test that getTrainDeparturesByDepartureTime returns
     * correct values on valid input.
     */
    @Test
    @DisplayName("getTrainDeparturesByDepartureTime "
        + "returns correct values on valid input")
    void testGetTrainDeparturesByDepartureTimeOnValidInput() {
      ArrayList<TrainDeparture> time1020Trains;
      time1020Trains = new ArrayList<>();
      time1020Trains.add(train1);
      assertEquals(time1020Trains,
          register.getTrainDeparturesByDepartureTime(LocalTime.of(10, 20)));
    }

    /**
     * Test that getSortedRegisterByTime returns correctly sorted list.
     */
    @Test
    @DisplayName("getSortedRegisterByTime returns correctly sorted list")
    void testGetSortedRegisterByTime() {
      ArrayList<TrainDeparture> sortedList = new ArrayList<>();
      sortedList.add(train1);
      sortedList.add(train3);
      sortedList.add(train4);
      sortedList.add(train2);
      sortedList.add(train5);
      assertEquals(sortedList, register.getSortedRegisterByTime());
    }

    /**
     * Test that removeDepartedTrains correctly removes departed trains.
     */
    @Test
    @DisplayName("removeDepartedTrains correctly removes departed trains.")
    void testRemoveDepartedTrains() {
      time.setTime(LocalTime.of(18, 30));
      register.removeDepartedTrains();
      assertEquals(2, register.getTrainDepartures().size());
      time.setTime(LocalTime.of(23, 59));
      register.removeDepartedTrains();
      assertEquals(0, register.getTrainDepartures().size());
    }

    /**
     * Test that assignTrackNumber correctly assigns track numbers
     * to a train departure.
     */
    @Test
    @DisplayName("assignTrackNumber correctly"
        + " assigns track numbers to a train departure.")
    void testAssignTrackNumber() {
      register.assignTrackNumber(1, 1);
      assertEquals(1, train1.getTrackNumber());
      register.assignTrackNumber(2, 2);
      assertEquals(2, train2.getTrackNumber());
      register.assignTrackNumber(3, 3);
      assertEquals(3, train3.getTrackNumber());
      register.assignTrackNumber(4, 4);
      assertEquals(4, train4.getTrackNumber());
      register.assignTrackNumber(5, 5);
      assertEquals(5, train5.getTrackNumber());
    }
  }

  /**
   * Nested class to do negative tests on the public TrainRegister methods.
   */
  @Nested
  @DisplayName("Negative tests for TrainRegister methods.")
  class MethodsThrowExceptions {

    /**
     * Test that registerNewTrainDeparture throws
     * IllegalArgumentException on invalid input.
     */
    @Test
    @DisplayName("registerNewTrainDeparture throws"
        + " IllegalArgumentException on invalid input.")
    void testRegisterNewDepartureOnInvalidInput() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(0, 0));
      TrainRegister register = new TrainRegister(new ArrayList<>(), time);
      assertThrows(IllegalArgumentException.class,
          () -> register.registerNewTrainDeparture(null));
    }

    /**
     * Test that getTrainDeparturesByDestination throws
     * NullPointerException on invalid input.
     */
    @Test
    @DisplayName("getTrainDeparturesByDestination throws "
        + "NullPointerException on invalid input")
    void testGetTrainDeparturesByDestinationOnInvalidInput() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(0, 0));
      TrainRegister register = new TrainRegister(new ArrayList<>(), time);
      assertThrows(NullPointerException.class,
          () -> register.getTrainDeparturesByDestination(null));
      assertThrows(NullPointerException.class,
          () -> register.getTrainDeparturesByDestination(""));
    }

    /**
     * Test that getTrainDepartureByTrainNumber throws
     * IllegalArgumentException on invalid input.
     */
    @Test
    @DisplayName("getTrainDepartureByTrainNumber "
        + "throws IllegalArgumentException on invalid input")
    void testGetTrainDepartureByTrainNumberOnInvalidInput() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(0, 0));
      TrainRegister register = new TrainRegister(new ArrayList<>(), time);
      assertThrows(IllegalArgumentException.class,
          () -> register.getTrainDepartureByTrainNumber(-1));
      assertThrows(IllegalArgumentException.class,
          () -> register.getTrainDepartureByTrainNumber(0));
    }

    /**
     * Test that getTrainDepartureByTrainNumber
     * returns null on non-existent train number.
     */
    @Test
    @DisplayName("getTrainDepartureByTrainNumber "
        + "returns null on non-existent train number")
    void testGetTrainDepartureReturnsNullOnNonExistentTrainNumber() {
      CurrentTimeHandler time = new CurrentTimeHandler(LocalTime.of(0, 0));
      TrainRegister register = new TrainRegister(new ArrayList<>(), time);
      assertNull(register.getTrainDepartureByTrainNumber(1));
    }
  }

  /**
   * Nested class to test getters.
   */
  @Nested
  @DisplayName("Class to test getters.")
  class TestGetters {

    /**
     * Test variable for the TrainRegister object for the
     * MethodsDoNotThrowExceptions class to use throughout tests.
     */
    private TrainRegister register;

    /**
     * Test variable for the CurrentTimeHandler object for the
     * MethodsDoNotThrowExceptions class to use throughout tests.
     */
    private CurrentTimeHandler time;

    /**
     * Test TrainDeparture object 1 to use in tests.
     */
    private TrainDeparture train1;

    /**
     * Test TrainDeparture object 2 to use in tests.
     */
    private TrainDeparture train2;

    /**
     * Test TrainDeparture object 3 to use in tests.
     */
    private TrainDeparture train3;

    /**
     * Test TrainDeparture object 4 to use in tests.
     */
    private TrainDeparture train4;

    /**
     * Test TrainDeparture object 5 to use in tests.
     */
    private TrainDeparture train5;


    /**
     * Initialize test objects.
     */
    @BeforeEach
    void init() {
      time = new CurrentTimeHandler(LocalTime.of(0, 0));
      register = new TrainRegister(new ArrayList<>(), time);
      train1 =
          new TrainDeparture(LocalTime.of(10, 20),
              "L4", 1, "Trondheim", 1, LocalTime.of(0, 10));
      train2 =
          new TrainDeparture(LocalTime.of(20, 10),
              "L4", 2, "Trondheim", 1, LocalTime.of(0, 0));
      train3 =
          new TrainDeparture(LocalTime.of(14, 25),
              "L21", 3, "Oslo", 1, LocalTime.of(0, 0));
      train4 =
          new TrainDeparture(LocalTime.of(17, 45),
              "L26", 4, "Bergen", 1, LocalTime.of(0, 5));
      train5 =
          new TrainDeparture(LocalTime.of(22, 20),
              "L4", 5, "Stavanger", 1, LocalTime.of(1, 30));
      register.registerNewTrainDeparture(train1);
      register.registerNewTrainDeparture(train2);
      register.registerNewTrainDeparture(train3);
      register.registerNewTrainDeparture(train4);
      register.registerNewTrainDeparture(train5);
    }

    /**
     * Test that getTrainDepartures correctly
     * returns the arraylist of train departures.
     */
    @Test
    @DisplayName("Test getTrainDepartures.")
    void testGetTrainDepartures() {
      ArrayList<TrainDeparture> testList = new ArrayList<>();
      testList.add(train1);
      testList.add(train2);
      testList.add(train3);
      testList.add(train4);
      testList.add(train5);
      assertEquals(testList, register.getTrainDepartures());
    }

    /**
     * Test that getTime correctly returns the time object.
     */
    @Test
    @DisplayName("Test getTime.")
    void testGetTime() {
      assertEquals(time, register.getTime());
    }
  }
}
