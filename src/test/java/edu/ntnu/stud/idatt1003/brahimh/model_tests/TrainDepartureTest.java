package edu.ntnu.stud.idatt1003.brahimh.model_tests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.idatt1003.brahimh.model.TrainDeparture;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Class to test the TrainDeparture class.
 *
 * @author Brahim Helland
 * @version 1.0
 */

class TrainDepartureTest {

  /**
   * Nested class to do negative tests on the TrainDeparture constructor.
   */
  @Nested
  @DisplayName("Negative tests TrainDeparture constructor,"
      + " throws IllegalArgumentException or java.time.DateTimeException"
      + " on invalid input.")
  class MethodsThrowExceptions {

    /**
     * Test variable for the departure time to use throughout tests.
     */
    private LocalTime testDepartureTime;

    /**
     * Test variable for the delay to use throughout tests.
     */
    private LocalTime testDelay;

    @BeforeEach
    void init() {
      testDepartureTime = LocalTime.of(12, 30);
      testDelay = LocalTime.of(0, 10);
    }

    @Test
    @DisplayName("TrainDeparture constructor throws"
        + " IllegalArgumentException on blank train line.")
    void testTrainDepartureConstructorOnBlankTrainLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "", 1, "Trondheim", 1, testDelay)
      );
    }

    /**
     * Test that the constructor throws
     * IllegalArgumentException on null train line.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "IllegalArgumentException on null train line.")
    void testTrainDepartureConstructorOnNullTrainLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, null, 1, "Trondheim", 1, testDelay)
      );
    }

    /**
     * Test that the constructor throws IllegalArgumentException
     * on negative train number.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "IllegalArgumentException on negative train number.")
    void testTrainDepartureConstructorOnNegativeTrainNumber() {

      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", -1, "Trondheim", 1, testDelay)
      );
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", -4, "Trondheim", 1, testDelay)
      );
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", -12471247, "Trondheim", 1, testDelay)
      );
    }

    /**
     * Test that the constructor throws
     * IllegalArgumentException on blank destination.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "NullPointerException on blank destination.")
    void testTrainDepartureConstructorOnBlankDestination() {

      assertThrows(NullPointerException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", 1, "", 1, testDelay)
      );
    }

    /**
     * Test that the constructor throws
     * NullPointerException on null destination.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "NullPointerException on null destination.")
    void testTrainDepartureConstructorOnNullDestination() {
      assertThrows(NullPointerException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", 1, null, 1, testDelay)
      );
    }

    /**
     * Test that the constructor throws IllegalArgumentException
     * on negative track number, ignoring -1.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "IllegalArgumentException on negative track number (except -1).")
    void testTrainDepartureConstructorOnNegativeTrackNumber() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", 1, "Trondheim", -2, testDelay)
      );
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", 1, "Trondheim", -5, testDelay)
      );
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", 1, "Trondheim", -248524231, testDelay)
      );
    }

    /**
     * Test that the constructor throws java.time.DateTimeException
     * on negative delay.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "java.time.DateTimeException on negative delay.")
    void testTrainDepartureConstructorOnNegativeDelay() {
      assertThrows(java.time.DateTimeException.class, () -> new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(-1, 0))
      );
    }

    /**
     * Test that the constructor throws IllegalArgumentException on null delay.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "IllegalArgumentException on null delay.")
    void testTrainDepartureConstructorOnNullDelay() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          testDepartureTime, "L4", 1, "Trondheim", 1, null)
      );
    }

    /**
     * Test that the constructor throws DateTimeException on
     * negative departure time.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "DateTimeException on negative departure time.")
    void testTrainDepartureConstructorOnNegativeDepartureTime() {
      assertThrows(java.time.DateTimeException.class, () -> new TrainDeparture(
          LocalTime.of(-1, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10))
      );
    }

    /**
     * Test that the constructor throws
     * IllegalArgumentException on null departure time.
     */
    @Test
    @DisplayName("TrainDeparture constructor throws "
        + "IllegalArgumentException on null departure time.")
    void testTrainDepartureConstructorOnNullDepartureTime() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          null, "L4", 1, "Trondheim", 1, testDelay)
      );
    }
  }

  /**
   * Nested class to do positive tests on the TrainDeparture constructor.
   * Test that constructor does not throw exceptions on valid inputs.
   */
  @Nested
  @DisplayName("Positive tests TrainDeparture constructor,"
      + " does not throw IllegalArgumentException "
      + "or java.time.DateTimeException on valid input.")
  class MethodsDoNotThrowExceptions {

    /**
     * Test that the constructor does not throw exceptions on valid input.
     */
    @Test
    @DisplayName("TrainDeparture constructor does not throw "
        + "IllegalArgumentException on valid input.")
    void testTrainDepartureConstructorOnValidInput() {
      assertDoesNotThrow(() -> new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10))
      );
    }
  }

  /**
   * Nested class for positive tests for setters and getters for TrainDeparture.
   */
  @Nested
  @DisplayName("Test setters and getters for TrainDeparture.")
  class ConstructorTest {

    /**
     * Test that getDepartureTime correctly returns the departure time.
     */
    @Test
    @DisplayName("Test getDepartureTime.")
    void testGetDepartureTime() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      assertEquals(LocalTime.of(12, 30), train1.getDepartureTime());
    }

    /**
     * Test that getTrainLine correctly returns the train line.
     */
    @Test
    @DisplayName("Test getTrainLine.")
    void testGetTrainLine() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      assertEquals("L4", train1.getTrainLine());
    }

    /**
     * Test that getTrainNumber correctly returns the train number.
     */
    @Test
    @DisplayName("Test getTrainNumber.")
    void testGetTrainNumber() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      assertEquals(1, train1.getTrainNumber());
    }

    /**
     * Test that getDestination correctly returns the destination.
     */
    @Test
    @DisplayName("Test getDestination.")
    void testGetDestination() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      assertEquals("Trondheim", train1.getDestination());
    }

    /**
     * Test that getTrackNumber correctly returns the track number.
     */
    @Test
    @DisplayName("Test getTrackNumber.")
    void testGetTrackNumber() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      assertEquals(1, train1.getTrackNumber());
    }

    /**
     * Test that getDelay correctly returns the delay.
     */
    @Test
    @DisplayName("Test getDelay.")
    void testGetDelay() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      assertEquals(LocalTime.of(0, 10), train1.getDelay());
    }

    /**
     * Test that setTrackNumber correctly sets the track number.
     */
    @Test
    @DisplayName("Test setTrackNumber.")
    void testSetTrackNumber() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10)
      );
      train1.setTrackNumber(2);
      assertEquals(2, train1.getTrackNumber());
    }

    /**
     * Test that setDelay correctly sets the delay.
     */
    @Test
    @DisplayName("Test setDelay.")
    void testSetDelay() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", -1, LocalTime.of(0, 0)
      );
      train1.setDelay(LocalTime.of(0, 20));
      assertEquals(LocalTime.of(0, 20), train1.getDelay());
    }

    /**
     * Test that the getDelayedDepartureTime method correctly
     * calculates and returns the departure time after delay.
     */
    @Test
    @DisplayName("Test getDelayedDepartureTime.")
    void testGetDelayedDepartureTime() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", -1, LocalTime.of(0, 30)
      );
      assertEquals(LocalTime.of(13, 0), train1.getDelayedDepartureTime());
    }
  }

  @Nested
  @DisplayName("Test 'get as string' methods.")
  class ToStringTest {

    /**
     * Test that the toString method returns the correct string.
     */
    @Test
    @DisplayName("Test getDelayAsString.")
    void testGetDelayAsString() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", -1, LocalTime.of(0, 30)
      );

      TrainDeparture train2 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", -1, LocalTime.of(0, 0)
      );

      assertEquals("30 min", train1.getDelayAsString());
      assertEquals("", train2.getDelayAsString());
    }

    @Test
    @DisplayName("Test getTrackNumberAsString.")
    void testGetTrackNrAsString() {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", -1, LocalTime.of(0, 30)
      );

      TrainDeparture train2 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 0)
      );

      assertEquals("", train1.getTrackNumberAsString());
      assertEquals("1", train2.getTrackNumberAsString());
    }
  }
}
