/**
 * This package contains the main class of the application.
 * <p>
 * This is where the app is launched.
 * </p>
 */

package edu.ntnu.stud.idatt1003.brahimh.app_launcher;
