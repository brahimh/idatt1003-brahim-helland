/**
 * This package implements an application that handles train departures. The
 *     application is a simple version of a train dispatch system. The package
 *     contains the following subpackages: applauncher, model and visual.
 *     The applauncher package contains the main class for the application.
 *     The model package contains the model classes for the application such as
 *     train departure class and the train register class as well as the class
 *     for handling time. The visual package contains the classes for the user
 *     interface and the menu printer.
 */

package edu.ntnu.stud.idatt1003.brahimh;


