package edu.ntnu.stud.idatt1003.brahimh.model;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

/**
 * Class to handle the current time for the application.
 *
 * <p>
 * This class is used to set and get the current time for the application.
 * It also has a method to validate the time input. It is used in both the
 * TrainRegister class and the UserInterface class.
 * </p>
 *
 * @author Brahim Helland
 * @version 1.0
 */
public class CurrentTimeHandler {

  /**
   * The current time for the application.
   */
  private LocalTime currentTime;

  /**
   * Constructor for the CurrentTimeHandler class.
   *
   * @param currentTime The current time for the application.
   */
  public CurrentTimeHandler(final LocalTime currentTime) {
    this.currentTime = currentTime;
    this.setTime(currentTime);
  }

  /**
   * Method to sets the current time for the application.
   *
   * @param newTime The new time to be set for the application.
   */
  public void setTime(final LocalTime newTime) {
    if (newTime == null) {
      throw new NullPointerException("Time cannot be null.");
    } else if (newTime.isBefore(this.currentTime)) {
      throw new IllegalArgumentException(
          "New time cannot be before current time.");
    }
    this.currentTime = newTime;
  }

  /**
   * Gets the current time for the application.
   *
   * @return Returns the current time for the application.
   */
  public LocalTime getTime() {
    return this.currentTime;
  }

  /**
   * Method to validate the time input.
   *
   * @param time The time to be validated.
   * @throws DateTimeParseException If the time is not in the correct format.
   */
  public static void validateTimeInput(final String time)
      throws DateTimeParseException {
    try {
      LocalTime.parse(time);
    } catch (DateTimeParseException e) {
      throw new DateTimeParseException("Time is not in the correct format: "
          + time, time, 0);
    }
  }
}
