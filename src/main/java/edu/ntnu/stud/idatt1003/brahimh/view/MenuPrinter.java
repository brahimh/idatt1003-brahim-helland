package edu.ntnu.stud.idatt1003.brahimh.view;

/**
 * Class to print the menus in the user interface.
 *
 * <p>
 *   This class contains methods to print the main menu and the search menu.
 *   It could potentially be expanded upon to add more menus as the program
 *   gets more advanced.
 * </p>
 *
 * @author Brahim Helland
 * @version 1.0
 */
public final class MenuPrinter {

  // Static variables for frequently used strings.
  private static final String CHOOSE_OPTION = ("Please choose an option:");

  private MenuPrinter() {
  }

  /**
   * Method that prints the main menu.
   *
   * @throws IllegalArgumentException if the user input is not an integer.
   */
  public static void showMainMenu() {
    System.out.println(CHOOSE_OPTION);
    System.out.println("""
        1: Register new train departure.
        2: Print all departures by time.
        3: Search for train departures.
        4: Assign track number to departure.
        5: Set delay to train departure.
        6: Update clock.
        7: Exit application.
        """);
  }

  /**
   * Method that prints the menu for searching for train departures.
   */
  public static void showSearchMenu() {
    System.out.println(CHOOSE_OPTION);
    System.out.println("""
        1: Search for train departure by train number.
        2: Search for train departures by destination.
        3: Search for train departures by train line.
        4: Search for train departures by track number.
        5: Search for train departures by departure time.
        6: Back to main menu.
        """);
  }
}
