package edu.ntnu.stud.idatt1003.brahimh.view;

import edu.ntnu.stud.idatt1003.brahimh.model.CurrentTimeHandler;
import edu.ntnu.stud.idatt1003.brahimh.model.TrainDeparture;
import edu.ntnu.stud.idatt1003.brahimh.model.TrainRegister;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This is the class for handling the user interface.
 *
 * <p>
 * The user interface is the class that handles the user input and output.
 * It also calls the methods from the TrainRegister class to perform the
 * different operations as well as the menu printer to print the menus.
 * </p>
 *
 * @author Brahim Helland
 * @version 1.0
 */
public final class UserInterface {

  // Declaring objects for the TrainRegister and CurrentTimeHandler classes.
  private static TrainRegister register;
  private static CurrentTimeHandler time;

  // Constant static choices for the main menu.
  private static final int REGISTER_NEW_TRAIN_DEPARTURE = 1;
  private static final int PRINT_ALL_DEPARTURES_BY_TIME = 2;
  private static final int SEARCH_FOR_TRAIN = 3;
  private static final int ASSIGN_TRACK_NUMBER_TO_DEPARTURE = 4;
  private static final int ADD_DELAY_TO_TRAIN_DEPARTURE = 5;
  private static final int UPDATE_CLOCK = 6;
  private static final int EXIT_APPLICATION = 7;

  // Constant static choices for the search menu.
  private static final int SEARCH_BY_TRAIN_NR = 1;
  private static final int SEARCH_BY_DESTINATION = 2;
  private static final int SEARCH_BY_LINE = 3;
  private static final int SEARCH_BY_TRACK_NR = 4;
  private static final int SEARCH_BY_DEPARTURE_TIME = 5;
  private static final int GO_BACK = 6;

  // Constant static variables for frequently used strings.
  private static final String ENTER_TRAIN_NR = "Enter train number: ";
  private static final String ENTER_TRACK_NR = "Enter track number: ";
  private static final String SEARCHING_FOR_DEPT =
      "Searching for departure(s)...";


  // Constant static variables for frequently used exception messages.
  private static final String ILLEGAL_TRAIN_NUMBER =
      "Train number must be an integer.";
  private static final String ILLEGAL_TRACK_NUMBER =
      "Track number must be an integer.";
  private static final String ILLEGAL_TIME_FORMAT =
      "Invalid time format, try again.";
  private static final String ILLEGAL_Y_OR_N_INPUT =
      "Invalid input. Enter 'y' or 'n'.";
  private static final String ILLEGAL_ARGUMENT =
      "Unexpected value, try again.";
  private static final String GENERAL_EXCEPTION_MESSAGE =
      "Something went wrong, try again.";

  /**
   * Creating new Scanner object for user input.
   */
  private static final Scanner Scan = new Scanner(System.in);

  private UserInterface() {
    throw new AssertionError("Utility class should not be instantiated");
  }

  /**
   * Method to initialize test data.
   */
  private static void registerTestData() {
    try {
      TrainDeparture train1 = new TrainDeparture(
          LocalTime.of(12, 30), "L4", 1, "Trondheim", 1, LocalTime.of(0, 10));
      TrainDeparture train2 = new TrainDeparture(
          LocalTime.of(12, 20), "L1", 2, "Ås", 2, LocalTime.of(0, 0));
      TrainDeparture train3 = new TrainDeparture(
          LocalTime.of(13, 0), "L2", 3, "Bergen", 3, LocalTime.of(0, 0));
      TrainDeparture train4 = new TrainDeparture(
          LocalTime.of(13, 15), "F4", 4, "Stavanger", 4, LocalTime.of(0, 0));
      TrainDeparture train5 = new TrainDeparture(
          LocalTime.of(13, 30), "L4", 5, "Trondheim", 5, LocalTime.of(0, 0));

      register.registerNewTrainDeparture(train1);
      register.registerNewTrainDeparture(train2);
      register.registerNewTrainDeparture(train3);
      register.registerNewTrainDeparture(train4);
      register.registerNewTrainDeparture(train5);

    } catch (IllegalArgumentException | NullPointerException | DateTimeException e) {
      System.out.println("Item could not be added because of the following reason:"
          + e.getMessage());
    }
  }

  /**
   * Method to run the menu/user interface.
   */
  public static void runMenu() {
    boolean cancel = false;


    while (!cancel) {
      try {
        MenuPrinter.showMainMenu();
        if (Scan.hasNextInt()) {
          int menuChoice = Scan.nextInt();
          Scan.nextLine();
          switch (menuChoice) {
            case REGISTER_NEW_TRAIN_DEPARTURE -> userRegisterNewDeparture();
            case PRINT_ALL_DEPARTURES_BY_TIME -> register.printAllDeparturesByTime();
            case SEARCH_FOR_TRAIN -> runSearchMenu();
            case ASSIGN_TRACK_NUMBER_TO_DEPARTURE -> userAssignTrackNumberToDeparture();
            case ADD_DELAY_TO_TRAIN_DEPARTURE -> userAddDelayToTrainDeparture();
            case UPDATE_CLOCK -> userUpdateClock();
            case EXIT_APPLICATION -> {
              System.out.println("Exiting application...");
              cancel = true;
            }
            default -> throw new IllegalArgumentException(ILLEGAL_ARGUMENT);
          }
        } else {
          System.out.println("Invalid input. Please enter a valid integer.");
          Scan.nextLine();
        }
      } catch (IllegalArgumentException
               | IllegalStateException
               | NullPointerException e) {
        System.out.println(e.getMessage());
      } catch (DateTimeParseException e) {
        System.out.println(ILLEGAL_TIME_FORMAT);
      } catch (Exception e) {
        System.out.println(GENERAL_EXCEPTION_MESSAGE);
      }
    }
  }

  private static void runSearchMenu() {

    boolean cancel = false;

    while (!cancel) {
      MenuPrinter.showSearchMenu();
      int searchMenuChoice = Scan.nextInt();
      Scan.nextLine();
      switch (searchMenuChoice) {
        case SEARCH_BY_TRAIN_NR -> userSearchForTrainDepartureByTrainNumber();
        case SEARCH_BY_DESTINATION -> userSearchForTrainDeparturesByDestination();
        case SEARCH_BY_LINE -> userSearchForTrainDeparturesByTrainLine();
        case SEARCH_BY_TRACK_NR -> userSearchForTrainDeparturesByTrackNumber();
        case SEARCH_BY_DEPARTURE_TIME -> userSearchForTrainDeparturesByDepartureTime();
        case GO_BACK -> {
          System.out.println("Going back...");
          cancel = true;
        }
        default -> throw new IllegalArgumentException(ILLEGAL_ARGUMENT);
      }
    }
  }

  /**
   * Method to launch the user interface and application.
   */
  public static void launch() {
    time = new CurrentTimeHandler(LocalTime.of(0, 0));
    register = new TrainRegister(new ArrayList<>(), time);
    System.out.println("Welcome to the train dispatch application! Current time is: "
        + time.getTime());
    registerTestData();
    runMenu();
  }

  /**
   * Method to let the user register new train departure data.
   */
  private static void userRegisterNewDeparture() {
    System.out.println("Enter departure time (hh:mm): ");
    String departureTime = Scan.nextLine();
    CurrentTimeHandler.validateTimeInput(departureTime);

    System.out.println("Enter train line: ");
    final String trainLine = Scan.nextLine();

    System.out.println(ENTER_TRAIN_NR);
    int trainNumber;
    try {
      trainNumber = Scan.nextInt();
    } catch (InputMismatchException e) {
      Scan.nextLine(); // Consume the invalid input.
      throw new IllegalArgumentException(ILLEGAL_TRAIN_NUMBER);
    }
    Scan.nextLine(); // Consume the new newline character.

    System.out.println("Enter destination: ");
    final String destination = Scan.nextLine();

    System.out.println("Would you like to enter a track number? (y/n)");
    String trackNumberChoice = Scan.nextLine().toLowerCase();
    int trackNumber;
    if (trackNumberChoice.equals("y")) {
      System.out.println(ENTER_TRACK_NR);
      try {
        trackNumber = Scan.nextInt();
      } catch (InputMismatchException e) {
        Scan.nextLine(); // Consume the invalid input.
        throw new IllegalArgumentException(ILLEGAL_TRACK_NUMBER);
      }
      Scan.nextLine(); // Consume the new newline character.
    } else if (trackNumberChoice.equals("n")) {
      System.out.println("Track number not assigned.");
      trackNumber = -1;
    } else {
      throw new IllegalArgumentException(ILLEGAL_Y_OR_N_INPUT);
    }

    System.out.println("Would you like to add a delay? (y/n)");
    String delayChoice = Scan.nextLine().toLowerCase();
    LocalTime delay;
    if (delayChoice.equals("y")) {

      System.out.println("Enter delay (hh:mm): ");
      String delayString = Scan.nextLine();
      CurrentTimeHandler.validateTimeInput(delayString);
      delay = LocalTime.parse(delayString);

    } else if (delayChoice.equals("n")) {
      delay = LocalTime.of(0, 0);
      System.out.println("Delay not added.");
    } else {
      throw new IllegalArgumentException(ILLEGAL_Y_OR_N_INPUT);
    }

    TrainDeparture train =
        new TrainDeparture(LocalTime.parse(departureTime), trainLine,
            trainNumber, destination, trackNumber, delay);
    register.registerNewTrainDeparture(train);

    System.out.println("New train departure registered:");

    register.printSingleDeparture(train);
  }

  /**
   * Method to let the user search for train departures by destination.
   */
  private static void userSearchForTrainDepartureByTrainNumber() {
    System.out.println(ENTER_TRAIN_NR);
    int trainNumber;
    try {
      trainNumber = Scan.nextInt();
    } catch (InputMismatchException e) {
      Scan.nextLine(); // Consume the invalid input.
      throw new IllegalArgumentException(ILLEGAL_TRAIN_NUMBER);
    }
    Scan.nextLine(); // Consume the new newline character.
    System.out.println(SEARCHING_FOR_DEPT);
    TrainDeparture departure =
        register.getTrainDepartureByTrainNumber(trainNumber);
    if (departure == null) {
      System.out.println(
          "No departure found with train number: " + trainNumber + ".");
    } else {
      register.printSingleDeparture(departure);
    }
  }

  /**
   * Method to let the user search for train departures by destination.
   */
  private static void userSearchForTrainDeparturesByDestination() {
    System.out.println("Enter destination: ");
    String destination = Scan.nextLine();
    System.out.println(SEARCHING_FOR_DEPT);
    register.printDeparturesArrayList(
        register.getTrainDeparturesByDestination(destination)
    );
  }

  /**
   * Method to let the user search for train departures by train line.
   */
  private static void userSearchForTrainDeparturesByTrainLine() {
    System.out.println("Enter train line: ");
    String trainLine = Scan.nextLine();
    System.out.println(SEARCHING_FOR_DEPT);
    register.printDeparturesArrayList(
        register.getTrainDeparturesByTrainLine(trainLine)
    );
  }

  /**
   * Method to let the user search for train departures by track number.
   */
  private static void userSearchForTrainDeparturesByTrackNumber() {
    System.out.println(ENTER_TRACK_NR);
    int trackNumber = Scan.nextInt();
    Scan.nextLine(); // Consume newline character.
    System.out.println(SEARCHING_FOR_DEPT);
    register.printDeparturesArrayList(
        register.getTrainDeparturesByTrackNumber(trackNumber)
    );
  }

  /**
   * Method to let the user search for train departures by departure time.
   */
  private static void userSearchForTrainDeparturesByDepartureTime() {
    System.out.println("Enter departure time (hh:mm): ");
    LocalTime departureTime = LocalTime.parse(Scan.nextLine());
    System.out.println(SEARCHING_FOR_DEPT);
    register.printDeparturesArrayList(
        register.getTrainDeparturesByDepartureTime(departureTime)
    );
  }

  /**
   * Method to let the user assign a track number to a train departure.
   */
  private static void userAssignTrackNumberToDeparture() {
    System.out.println(ENTER_TRAIN_NR);
    final int trainNumber;
    try {
      trainNumber = Scan.nextInt();
    } catch (InputMismatchException e) {
      Scan.nextLine(); // Consume the invalid input.
      throw new IllegalArgumentException(ILLEGAL_TRAIN_NUMBER);
    }
    Scan.nextLine();
    System.out.println(ENTER_TRACK_NR);
    final int trackNumber;
    try {
      trackNumber = Scan.nextInt();
    } catch (InputMismatchException e) {
      Scan.nextLine(); // Consume the invalid input.
      throw new IllegalArgumentException(ILLEGAL_TRACK_NUMBER);
    }
    Scan.nextLine();
    System.out.println("Assigning track number...");
    register.assignTrackNumber(trainNumber, trackNumber);
  }

  /**
   * Method to let the user add delay to a train departure.
   */
  private static void userAddDelayToTrainDeparture() {
    System.out.println(ENTER_TRAIN_NR);
    final int trainNumber;
    try {
      trainNumber = Scan.nextInt();
    } catch (InputMismatchException e) {
      Scan.nextLine(); // Consume the invalid input.
      throw new IllegalArgumentException(ILLEGAL_TRAIN_NUMBER);
    }
    Scan.nextLine();
    System.out.println("Enter delay (hh:mm): ");
    String delay = Scan.nextLine();
    System.out.println("Adding delay...");
    register.getTrainDepartureByTrainNumber(trainNumber)
        .setDelay(LocalTime.parse(delay));
  }

  /**
   * Method to let the user update the clock.
   */
  private static void userUpdateClock() {
    System.out.println("Enter new time (hh:mm): ");
    String newTime = Scan.nextLine();
    System.out.print("Updating clock...");
    time.setTime(LocalTime.parse(newTime));
    register.removeDepartedTrains();
    System.out.println(" The current time is now " + time.getTime() + ".\n");
  }
}
