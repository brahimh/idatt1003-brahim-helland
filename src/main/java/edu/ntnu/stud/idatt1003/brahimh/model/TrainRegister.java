package edu.ntnu.stud.idatt1003.brahimh.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * This is the class for the Train register.
 * <p>
 * This class represents a register of all train departures.
 * It keeps all train departures in an ArrayList and can
 * register new train departures, search for train departures,
 * print train departures, and assign track numbers to train departures.
 * </p>
 *
 * @author Brahim Helland
 * @version 1.0
 */
public class TrainRegister {

  // Static variables for frequently used strings.
  private static final String FORMAT_SPACING =
      "| %-14s | %-14s | %-14s | %-14s | %-14s | %-14s |%n";

  private static final String ROW_BREAK =
      "------------------------------------------------------"
          + "-------------------------------------------------%n";
  private static final String HEADER_WITH_TITLE =
      "--------------------------------------"
          + "Train Dispatch System---------------------------------------";

  private static final String COL_1_HEADER = "Departure Time";
  private static final String COL_2_HEADER = "Line";
  private static final String COL_3_HEADER = "Train Number";
  private static final String COL_4_HEADER = "Destination";
  private static final String COL_5_HEADER = "Track";
  private static final String COL_6_HEADER = "Delay";

  /**
   * <b>ArrayList</b> containing all train departures.
   */
  private ArrayList<TrainDeparture> trainDepartures;

  // Declares the time object for the application.
  private CurrentTimeHandler time;

  /**
   * Constructor for the TrainRegister class.
   *
   * @param trainDepartures ArrayList containing all train departures.
   * @param time            The current time for the application.
   */
  public TrainRegister(final ArrayList<TrainDeparture> trainDepartures,
                       final CurrentTimeHandler time) {
    this.setTrainDepartures(trainDepartures);
    this.setCurrentTimeHandler(time);
  }

  /**
   * Sets the train departures arraylist.
   *
   * @param trainDepartures ArrayList containing all train departures.
   */
  private void setTrainDepartures(
      final ArrayList<TrainDeparture> trainDepartures) {
    if (trainDepartures == null) {
      throw new IllegalArgumentException(
          "Train departures arraylist cannot be null");
    }
    this.trainDepartures = trainDepartures;
  }

  private void setCurrentTimeHandler(final CurrentTimeHandler time) {
    if (time == null) {
      throw new IllegalArgumentException("Time cannot be null");
    }
    this.time = time;
  }

  /**
   * Registers a new train departure and checks for illegal arguments.
   *
   * @param trainDeparture The train departure to be registered.
   */
  public final void registerNewTrainDeparture(final TrainDeparture
                                                  trainDeparture) {

    TrainDeparture.validateTrainDeparture(trainDeparture);

    if (getTrainDepartureByTrainNumber(
        trainDeparture.getTrainNumber()) != null) {
      throw new IllegalArgumentException("Train number already exists");
    }
    trainDepartures.add(trainDeparture);
  }


  /**
   * Gets all train departures.
   *
   * @return Returns the list of all train departures as an arraylist.
   */
  public ArrayList<TrainDeparture> getTrainDepartures() {
    return this.trainDepartures;
  }

  /**
   * Gets the time object for the application.
   *
   * @return Returns the time object for the application.
   */
  public CurrentTimeHandler getTime() {
    return this.time;
  }

  /**
   * Prints all train departures in a nicely formatted manner.
   */
  public void printAllDeparturesByTime() {
    System.out.println("Showing all departures...");
    System.out.print(time.getTime() + HEADER_WITH_TITLE + "\n");
    System.out.printf(FORMAT_SPACING,
        COL_1_HEADER,
        COL_2_HEADER,
        COL_3_HEADER,
        COL_4_HEADER,
        COL_5_HEADER,
        COL_6_HEADER);

    System.out.printf(ROW_BREAK);
    for (TrainDeparture trainDeparture : getSortedRegisterByTime()) {
      System.out.printf(FORMAT_SPACING,
          trainDeparture.getDepartureTime(),
          trainDeparture.getTrainLine(),
          trainDeparture.getTrainNumber(),
          trainDeparture.getDestination(),
          trainDeparture.getTrackNumberAsString(),
          trainDeparture.getDelayAsString());
    }
    System.out.printf(ROW_BREAK);
  }

  /**
   * Uses the trainDeparture class to sort the register by time.
   *
   * @return Returns a list sorted by the trains' departure
   *     times in ascending order as an ArrayList.
   */

  public ArrayList<TrainDeparture> getSortedRegisterByTime() {
    return trainDepartures.stream()
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
  }

  /**
   * Gets the train departure for a given train number.
   *
   * @param trainNumber The train line to get all departures from.
   * @return Returns the train departure with the given
   *     number if found, if no train is found, it returns null.
   */
  public final TrainDeparture getTrainDepartureByTrainNumber(
      final int trainNumber) {

    TrainDeparture.validateTrainNumber(trainNumber);

    return this.trainDepartures.stream()
        .filter(trainDeparture ->
            trainDeparture.getTrainNumber() == trainNumber)
        .findFirst()
        .orElse(null);
  }

  /**
   * Gets a list of all train departures by a given destination.
   *
   * @param destination The destination to get all departures from.
   * @return Returns a ArrayList of all train departures with the given
   *     destination.
   */
  public final ArrayList<TrainDeparture> getTrainDeparturesByDestination(
      final String destination) {

    TrainDeparture.validateDestination(destination);

    ArrayList<TrainDeparture> trainDeparturesByDestination =
        this.trainDepartures.stream()
            .filter(trainDeparture ->
                trainDeparture.getDestination().equalsIgnoreCase(destination))
            .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    if (trainDeparturesByDestination.isEmpty()) {
      throw new NullPointerException("No departures found for destination.");
    } else {
      return trainDeparturesByDestination;
    }
  }

  /**
   * Gets a list of all train departures by a given train line.
   *
   * @param trainLine The train line to get all departures for.
   * @return Returns a list of all train departures with the given train line
   */
  public final ArrayList<TrainDeparture> getTrainDeparturesByTrainLine(
      final String trainLine) {

    TrainDeparture.validateTrainLine(trainLine);

    ArrayList<TrainDeparture> trainDeparturesByTrainLine =
        this.trainDepartures.stream()
            .filter(trainDeparture ->
                trainDeparture.getTrainLine().equalsIgnoreCase(trainLine))
            .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    if (trainDeparturesByTrainLine.isEmpty()) {
      throw new NullPointerException("No departures found for train line");
    } else {
      return trainDeparturesByTrainLine;
    }
  }

  /**
   * Gets a list of all train departures by a given track number.
   *
   * @param trackNumber The track number to get all departures for.
   * @return Returns a list of all train departures with the given track number
   */
  public final ArrayList<TrainDeparture> getTrainDeparturesByTrackNumber(
      final int trackNumber) {

    TrainDeparture.validateTrackNumber(trackNumber);

    ArrayList<TrainDeparture> trainDeparturesByTrackNumber;

    trainDeparturesByTrackNumber = this.trainDepartures.stream()
        .filter(trainDeparture ->
            trainDeparture.getTrackNumber() == trackNumber)
        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    if (trainDeparturesByTrackNumber.isEmpty()) {
      throw new NullPointerException("No departures found for track number");
    } else {
      return trainDeparturesByTrackNumber;
    }
  }

  /**
   * Gets a list of all train departures by a given departure time.
   *
   * @param departureTime The departure time to get all departures for.
   * @return Returns a list of all train departures with the given
   *     departure time.
   */
  public final ArrayList<TrainDeparture> getTrainDeparturesByDepartureTime(
      final LocalTime departureTime) {

    TrainDeparture.validateDepartureTime(departureTime);

    ArrayList<TrainDeparture> trainDeparturesByDepartureTime =
        this.trainDepartures.stream()
            .filter(trainDeparture ->
                trainDeparture.getDepartureTime().equals(departureTime))
            .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    if (trainDeparturesByDepartureTime.isEmpty()) {
      throw new NullPointerException("No departures found for departure time");
    } else {
      return trainDeparturesByDepartureTime;
    }
  }

  /**
   * Removes all departures that have already departed.
   */
  public final void removeDepartedTrains() {
    trainDepartures.removeIf(
        trainDeparture -> trainDeparture.getDelayedDepartureTime()
            .isBefore(time.getTime()));
  }

  /**
   * Assigns a track number to a train departure.
   *
   * @param trainNumber The train number to assign a track number to.
   * @param trackNumber The track number to assign to the train departure.
   */
  public final void assignTrackNumber(final int trainNumber,
                                      final int trackNumber) {

    TrainDeparture.validateTrainNumber(trainNumber);
    TrainDeparture.validateTrackNumber(trackNumber);

    TrainDeparture trainDeparture =
        getTrainDepartureByTrainNumber(trainNumber);
    if (trainDeparture != null) {
      trainDeparture.setTrackNumber(trackNumber);
    } else {
      throw new NullPointerException("Train number does not exist");
    }
  }

  /**
   * Prints a single train departure in a nicely formatted manner.
   *
   * @param trainDeparture The train departure to be printed.
   */
  public final void printSingleDeparture(final TrainDeparture trainDeparture) {

    TrainDeparture.validateTrainDeparture(trainDeparture);

    System.out.printf(ROW_BREAK);
    System.out.printf(FORMAT_SPACING,
        COL_1_HEADER,
        COL_2_HEADER,
        COL_3_HEADER,
        COL_4_HEADER,
        COL_5_HEADER,
        COL_6_HEADER);

    System.out.printf(ROW_BREAK);
    System.out.printf(FORMAT_SPACING,
        trainDeparture.getDepartureTime(),
        trainDeparture.getTrainLine(),
        trainDeparture.getTrainNumber(),
        trainDeparture.getDestination(),
        trainDeparture.getTrackNumberAsString(),
        trainDeparture.getDelayAsString());
    System.out.printf(ROW_BREAK);
  }

  /**
   * Prints all departures within given ArrayList in a nicely formatted manner.
   *
   * @param departures The list of departures to print.
   */
  public final void printDeparturesArrayList(
      final ArrayList<TrainDeparture> departures) {

    if (departures == null) {
      throw new NullPointerException("Departures arraylist is null");
    }

    System.out.printf(ROW_BREAK);
    System.out.printf(FORMAT_SPACING,
        COL_1_HEADER,
        COL_2_HEADER,
        COL_3_HEADER,
        COL_4_HEADER,
        COL_5_HEADER,
        COL_6_HEADER);

    System.out.printf(ROW_BREAK);
    for (TrainDeparture trainDeparture : departures) {
      System.out.printf(FORMAT_SPACING,
          trainDeparture.getDepartureTime(),
          trainDeparture.getTrainLine(),
          trainDeparture.getTrainNumber(),
          trainDeparture.getDestination(),
          trainDeparture.getTrackNumberAsString(),
          trainDeparture.getDelayAsString());
    }
    System.out.printf(ROW_BREAK);
  }
}
