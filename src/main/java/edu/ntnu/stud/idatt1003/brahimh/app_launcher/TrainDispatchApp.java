package edu.ntnu.stud.idatt1003.brahimh.app_launcher;

import edu.ntnu.stud.idatt1003.brahimh.view.UserInterface;

/**
 * This is the main class for the train dispatch application.
 *
 * <p>
 * This is where the user interface and by extension, application, is launched.
 * </p>
 *
 * @author Brahim Helland
 * @version 1.0
 */
public final class TrainDispatchApp {

  private TrainDispatchApp() {
  }

  /**
   * Main method to start the application.
   *
   * @param args are the arguments for the main-method.
   */
  public static void main(final String[] args) {
    UserInterface.launch();
  }
}
