
/**
 * Package for the visual part of the program.
 * <p>
 * This package contains the classes for the user interface and the
 * menu printer. The user interface is the class that handles the
 * user input and output. The menu printer is the class that prints
 * the different parts of the menu.
 * </p>
 */

package edu.ntnu.stud.idatt1003.brahimh.view;
