
/**
 * This package contains the model classes for the application.
 * <p>
 * The model classes are the classes that represent the data in the application.
 * The model classes are independent of the user interface and the
 * application launcher.
 * </p>
 */

package edu.ntnu.stud.idatt1003.brahimh.model;
