package edu.ntnu.stud.idatt1003.brahimh.model;

import java.time.LocalTime;

/**
 * This is the class represents individual train departures.
 * <p>
 * The class contains setters and getters for TrainDeparture parameters.
 * It also contains methods for validating the parameters that are used in
 * the setters and in the TrainRegister class as guard conditions.
 * </p>
 *
 * @author Brahim Helland
 * @version 1.0
 */

public class TrainDeparture {

  /**
   * Constant variable holding the number of minutes in an hour.
   */
  private static final int HOUR_IN_MINUTES = 60;

  /**
   * The departure time for the train.
   */
  private LocalTime departureTime;

  /**
   * The train line for the train.
   */
  private String trainLine;

  /**
   * The train number for the train.
   */
  private int trainNumber;

  /**
   * The destination for the train.
   */
  private String destination;

  /**
   * The track number for the train.
   */
  private int trackNumber;

  /**
   * The delay for the train.
   */
  private LocalTime delay;

  /**
   * Constructor for TrainDeparture object with the specified parameters.
   *
   * @param departureTime Departure time for given train in minutes and hours.
   * @param trainLine     Train line for given train.
   * @param destination   Destination for given train.
   * @param trackNumber   Track number for given train.
   * @param trainNumber   Train number for given train.
   * @param delay         Delay for given train in minutes
   * @throws IllegalArgumentException if any of the parameters are invalid.
   */
  public TrainDeparture(final LocalTime departureTime,
                        final String trainLine,
                        final int trainNumber,
                        final String destination,
                        final int trackNumber,
                        final LocalTime delay) {

    this.setDepartureTime(departureTime);
    this.setTrainLine(trainLine);
    this.setTrainNumber(trainNumber);
    this.setDestination(destination);
    this.setTrackNumber(trackNumber);
    this.setDelay(delay);
  }

  /**
   * Gets the departure time for the train.
   *
   * @return Returns the train's departure time, in hours and minutes.
   */
  public LocalTime getDepartureTime() {
    return this.departureTime;
  }

  /**
   * Sets the departure time for the train.
   *
   * @param departureTime as LocalTime.
   * @throws IllegalArgumentException if departure time is null or negative.
   */
  private void setDepartureTime(final LocalTime departureTime) {
    validateDepartureTime(departureTime);
    this.departureTime = departureTime;
  }

  /**
   * Gets the train line for the train.
   *
   * @return Returns the train's train line as a string.
   */
  public String getTrainLine() {
    return this.trainLine;
  }

  /**
   * Sets the train line for the train.
   *
   * @param trainLine as string.
   * @throws IllegalArgumentException if train line is null or empty.
   */

  private void setTrainLine(final String trainLine) {
    validateTrainLine(trainLine);
    this.trainLine = trainLine;
  }

  /**
   * Gets the train number for the train.
   *
   * @return Returns the train's train number as an integer.
   */

  public int getTrainNumber() {
    return this.trainNumber;
  }

  /**
   * Sets the train number for the train.
   *
   * @param trainNumber as integer.
   * @throws IllegalArgumentException if train number is less than or
   *                                  equal to 0.
   */
  private void setTrainNumber(final int trainNumber) {
    validateTrainNumber(trainNumber);
    this.trainNumber = trainNumber;
  }

  /**
   * Gets the destination for the train.
   *
   * @return Returns the train's destination as a string.
   */
  public String getDestination() {
    return this.destination;
  }

  /**
   * Sets the destination for the train.
   *
   * @param destination as string.
   * @throws IllegalArgumentException if destination is null or empty.
   */
  private void setDestination(final String destination) {
    validateDestination(destination);
    this.destination = destination;
  }

  /**
   * Gets the track number for the train.
   *
   * @return Returns the train's track number as an integer.
   */
  public int getTrackNumber() {
    return this.trackNumber;
  }

  /**
   * Sets the track number for the train.
   *
   * @param trackNumber as integer.
   * @throws IllegalArgumentException if track number is negative or 0.
   */
  public final void setTrackNumber(final int trackNumber) {
    validateTrackNumber(trackNumber);
    this.trackNumber = trackNumber;

  }

  /**
   * Gets the delay for the train departure.
   *
   * @return Returns the train's delay in minutes and hours (hh:mm).
   */
  public final LocalTime getDelay() {
    return this.delay;
  }

  /**
   * Sets the delay for the train departure.
   *
   * @param delay in minutes.
   * @throws IllegalArgumentException if delay is null or negative.
   */
  public final void setDelay(final LocalTime delay) {
    validateDelay(delay);
    this.delay = delay;
  }

  /**
   * Gets the new departure time for the train by adding the delay to
   * the regular departure time.
   *
   * @return Returns the train's delayed departure time, in hours and minutes.
   */
  public final LocalTime getDelayedDepartureTime() {
    return this.departureTime.plusMinutes(
        this.delay.getMinute()).plusHours(this.delay.getHour());
  }

  /**
   * Gets the delay as a string in the format: "m min".
   *
   * @return Returns the delay as a string
   * in the format "m min" or returns an empty string if delay is 00:00.
   */
  public final String getDelayAsString() {
    if (this.delay == LocalTime.of(0, 0)) {
      return "";
    } else {
      return this.delay.getHour() * HOUR_IN_MINUTES
          + this.delay.getMinute() + " min";
    }
  }

  /**
   * Gets the track number as a string, if the track number is yet to
   * be assigned (track number is -1) an empty string is returned.
   *
   * @return Returns the track number as a string or an empty
   * string if track number is -1.
   */
  public final String getTrackNumberAsString() {
    if (this.trackNumber == -1) {
      return "";
    } else {
      return Integer.toString(this.trackNumber);
    }
  }

  /**
   * Method to validate the departure time parameter. Also used
   * in TrainRegister class.
   * <p>
   * The departure time is validated by checking if it is null or negative.
   * </p>
   *
   * @param departureTime Departure time to be validated.
   */
  public static void validateDepartureTime(final LocalTime departureTime) {
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time cannot be null");
    } else if (departureTime.isBefore(LocalTime.of(0, 0))) {
      throw new IllegalArgumentException("Departure time cannot be negative");
    }
  }

  /**
   * Method to validate the train line parameter. Also used
   * in TrainRegister class.
   * <p>
   * The train line is validated by checking if it is null or empty.
   * </p>
   *
   * @param trainLine Train line to be validated.
   */
  public static void validateTrainLine(final String trainLine) {
    if (trainLine == null) {
      throw new IllegalArgumentException("Train line cannot be null");
    } else if (trainLine.isEmpty()) {
      throw new IllegalArgumentException("Train line cannot be empty");
    }
  }

  /**
   * Method to validate the train number parameter. Also used
   * in TrainRegister class.
   * <p>
   * The train number is validated by checking if it is
   * less than or equal to 0.
   * </p>
   *
   * @param trainNumber Train number to be validated.
   */
  public static void validateTrainNumber(final int trainNumber) {
    if (trainNumber <= 0) {
      throw new IllegalArgumentException(
          "Train number cannot be less than or equal to 0");
    }
  }

  /**
   * Method to validate the destination parameter. Also used
   * in TrainRegister class.
   * <p>
   * The destination is validated by checking if it is null or empty.
   * </p>
   *
   * @param destination Destination to be validated.
   */
  public static void validateDestination(final String destination) {
    if (destination == null) {
      throw new NullPointerException("Destination cannot be null");
    } else if (destination.isEmpty()) {
      throw new NullPointerException("Destination cannot be empty");
    }
  }

  /**
   * Method to validate the track number parameter. Also used
   * in TrainRegister class.
   * <p>
   * The track number is validated by checking if it is negative or 0.
   * </p>
   *
   * @param trackNumber Track number to be validated.
   */
  public static void validateTrackNumber(final int trackNumber) {

    if (trackNumber < 0 && trackNumber != -1) {
      throw new IllegalArgumentException("The only negative track number "
          + "allowed is -1");
    } else if (trackNumber == 0) {
      throw new IllegalArgumentException("Track number cannot be 0");
    }
  }

  /**
   * Method to validate the delay parameter. Also used
   * in TrainRegister class.
   * <p>
   * The delay is validated by checking if it is null or negative.
   * </p>
   *
   * @param delay Delay to be validated.
   */
  public static void validateDelay(final LocalTime delay) {
    if (delay == null) {
      throw new IllegalArgumentException("Delay cannot be null");
    } else if (delay.isBefore(LocalTime.of(0, 0))) {
      throw new IllegalArgumentException("Delay cannot be negative");
    }
  }

  /**
   * Method to validate all parameters for a train departure.
   *
   * <p>
   * The method calls all the other validation methods for the
   * individual parameters.
   * </p>
   *
   * @param trainDeparture Train departure to be validated.
   */
  public static void validateTrainDeparture(final TrainDeparture trainDeparture) {

    if (trainDeparture == null) {
      throw new IllegalArgumentException("Train departure cannot be null");
    }

    validateDepartureTime(trainDeparture.getDepartureTime());
    validateTrainLine(trainDeparture.getTrainLine());
    validateTrainNumber(trainDeparture.getTrainNumber());
    validateDestination(trainDeparture.getDestination());
    validateTrackNumber(trainDeparture.getTrackNumber());
    validateDelay(trainDeparture.getDelay());
  }
}
